fn split_into_string_array (contents: &str) -> Vec<&str> {
    let split: Vec<&str> = contents.split(",").map(|item| {
       item.trim()
    })
    .collect();
    split
}

pub fn create_array_of_strings(contents: &str) -> String {

    let array = split_into_string_array(contents);
    let mut formated_array: Vec<String> = Vec::new(); 
    for i in array {
        let word = add_quotes(i);

        formated_array.push(word);
    };
    let mut string = formated_array.join(",");

    string.insert(0, '[');
    string.push(']');
    string
}

fn add_quotes (list_item: &str) -> String {
    let item = list_item;
    let mut format = String::from(item);
    format.insert(0, '"');
    format.push('"');
    format.as_str().to_owned()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn split_the_contents() {
        let contents = "test1, test2, test3";

        assert_eq!(vec!["test1", "test2", "test3"], split_into_string_array(contents));
    }

    #[test]
    fn check_for_quotes(){
        let item = "test1";
        assert_eq!(r#""test1""#, add_quotes(item));
    }

    #[test]
    fn format_the_input(){
        let contents = "test1, test2, test3";
        assert_eq!(
        r#"["test1","test2","test3"]"#,
        create_array_of_strings(contents)
        )
    }
}

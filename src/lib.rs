use clap::Parser;
use std::{fs, process};

mod array;
mod object;
mod validators;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Args {
    /// Collection flag. Sets output collection type to an array. Must be used with a data flag.
    #[arg(short = 'A', long)]
    pub array: bool,

    /// Collection flag. Sets the output collection type to an non-JSON object. Doesn't need a data flag.
    #[arg(short = 'O', long)]
    pub object: bool,

    /// Data flag. Sets the type of data in the collection to a string type.
    #[arg(short, long)]
    pub string: bool,

    /// Data flag. Sets the type of data in the collection to a object type.
    #[arg(short, long)]
    pub object_data: bool,

    /// Path to the input file: Required.
    #[arg(short, long)]
    pub file: String,
}

/// Sets the initial configuration and calls the appropriate formatter based on the flags.
pub fn config(args: Args) {
    validators::validate_collection_flag(&args).unwrap_or_else(|err| {
        println!("{err}");
        process::exit(1);
    });

    validators::validate_data_flag(&args).unwrap_or_else(|err| {
        println!("{err}");
        process::exit(1);
    });

    println!("Reading file.");
    let path = args.file.clone();
    let contents = fs::read_to_string(&args.file).expect("Unable to read file.");

    let working_contents = contents.as_str();

    let completed_contents: String = match args {
        Args {
            array: true,
            object: false,
            ..
        } => array::create_array_of_strings(working_contents),
        Args {
            array: false,
            object: true,
            ..
        } => object::create_object(working_contents),
        _ => String::from("empty"),
    };

    println!("Formatting data from {path}");

    //TODO: creat output flag to be able to name the output file.
    fs::write("output.txt", completed_contents).expect("Unable to write to file.");
    println!("Format complete saved to output.txt");
}

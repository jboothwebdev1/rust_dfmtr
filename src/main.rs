use clap::Parser;
use dfmtr::{Args, config};

fn main() {
    let args = Args::parse();
    config(args);
}


fn split_for_object(contents: &str) -> Vec<&str> {
   let split: Vec<&str> = contents.lines().collect();
   split
}

pub fn create_object(contents: &str) -> String {
   let initial_array = split_for_object(contents);
   let mut complete_array: Vec<String> = Vec::new();
   for i in initial_array {
      let pair = split_pair(i);
      complete_array.push(pair);
   }

   let mut complete_string = complete_array.join("\n");
   complete_string.insert_str(0, "{\n");
   complete_string.push('\n');
   complete_string.push('}');

   complete_string
}

fn split_pair(item: &str) -> String {
   let mut key_values: Vec<&str> = item.split(',').collect();
   let mut key = String::from(key_values[0]);
   let mut value = String::from(key_values[1].trim());

   key.push(':');

   key_values[0] = key.as_str();

   if value.chars().all(char::is_numeric)|| value == "true" || value == "false"{
      value.push(',');
   } else {
      value.insert(0, '"');
      value.push('"');
      value.push(',');
   }

   key_values[1] = value.as_str();
   key_values.join(" ")
}


#[cfg(test)]
mod tests {
   use super::*;

   #[test]
   fn should_split_into_lines() {
      let contents = "foo, bar\nbar, foo\nbaz, paz\n";

      assert_eq!(vec!["foo, bar", "bar, foo", "baz, paz"], split_for_object(contents));
   }

   #[test]
   fn should_split_the_pair () {
      let contents = "foo, bar";

      assert_eq!(r#"foo: "bar","#, split_pair(contents));
   }
}

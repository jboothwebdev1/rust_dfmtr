use crate::Args;

pub fn validate_collection_flag(args: &Args) -> Result<(), &'static str> {
    match args {
        Args {
            array: true,
            object: false,
            ..
        } => Ok(()),
        Args {
            array: false,
            object: true,
            ..
        } => Ok(()),
        _ => Err("Missing Collection flag."),
    }
}

pub fn validate_data_flag(args: &Args) -> Result<(), &'static str> {
    match args {
        Args { string: true, .. } => Ok(()),
        Args { object: true, .. } => Ok(()),
        _ => Err("Missing data type flag."),
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn should_fail_collection_flag() {
        let item = Args {
            array: false,
            object: false,
            string: true,
            file: String::from("test"),
            object_data: false,
        };

        assert_eq!(
            Err("Missing Collection flag."),
            validate_collection_flag(&item)
        )
    }

    #[test]
    fn should_pass_collection_flag() {
        let item = Args {
            array: true,
            object: false,
            object_data: false,
            string: false,
            file: String::new(),
        };

        assert_eq!(Ok(()), validate_collection_flag(&item))
    }

    #[test]
    fn should_fail_data_flag() {
        let item = Args {
            array: true,
            object: false,
            object_data: false,
            string: false,
            file: String::from("test"),
        };

        assert_eq!(Err("Missing data type flag."), validate_data_flag(&item))
    }

    #[test]
    fn should_pass_data_flag() {
        let item = Args {
            array: false,
            object: false,
            object_data: false,
            string: true,
            file: String::new(),
        };

        assert_eq!(Ok(()), validate_data_flag(&item))
    }
}
